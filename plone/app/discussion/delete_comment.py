# -*- coding: utf-8 -*-
from Acquisition import aq_inner
from Acquisition import aq_parent
from Products.CMFPlone.utils import getToolByName

def can_delete_comment(context, comment):
    """Returns true if current user can delete the comment.
    """
    portal_membership = getToolByName(context, "portal_membership")
    authenticated_member = portal_membership.getAuthenticatedMember()
    is_creator = comment.Creator() == authenticated_member.getId()
    authorized = ['Moderador','Manager','Moderador_Master']
    roles = authenticated_member.getRolesInContext(comment)
    has_role = bool([i for i in roles if i in authorized])
    conversation = aq_parent(aq_inner(comment))
    has_replies = bool(conversation._children.get(int(comment.getId())))
    can_delete = has_role or (is_creator and not has_replies)
    return can_delete
